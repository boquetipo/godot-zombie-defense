extends Node

signal spawn_enemy
signal throw_grenade

var enemies:int = 0
var max_enemies:int = 20

var stick_deadzone:float = 0.3
var shooting_left:bool = false
var shooting_right:bool = false
var grenade_left:bool = false
var grenade_right:bool = false
var grenade_left_init:Vector2
var grenade_right_init:Vector2
var grenade_left_target:Vector2
var grenade_right_target:Vector2

var total_kills:int = 0
var total_points:int = 0

func get_max_enemies() -> int:
	return max_enemies

func get_ememy_count() -> int:
	return enemies

func modify_enemy_count(amount:int) -> void:
	enemies += amount

func get_shooting_left() -> bool:
	return shooting_left

func get_shooting_right() -> bool:
	return shooting_right

func get_shooting(side):
	match side:
		"left":
			return shooting_left
		"right":
			return shooting_right

func set_shooting(side, status:bool) -> void:
	match side:
		"left":
			shooting_left = status
		"right":
			shooting_right = status
