extends Node

onready var zombie = load("res://chars/zombie/Zombie.tscn")

var item_kills:Array = [25, 50, 75, 100]
var total_kills_l1:int = 0

func _ready():
	Events.connect("spawn_enemy", self, "_on_spawner_signal")

func _process(_delta):
	$Control_keys/Panel/HBoxContainer/axis_labels/count.text = str(Events.get_ememy_count());
	
func _on_Spawner_timeout():
	if Events.get_ememy_count() < Events.get_max_enemies():
		Events.modify_enemy_count(1)
		$YSort.add_child(zombie.instance())
		
#deprecated (button test)
func _on_spawner_signal():
	_on_Spawner_timeout()
