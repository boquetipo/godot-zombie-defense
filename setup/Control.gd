extends Control

func _ready():
	$Panel.hide()

func _physics_process(delta):
	if Input.is_action_pressed("exit"):
		get_tree().quit()

func _on_Button_pressed():
	print("spawn")
	Events.emit_signal("spawn_enemy")

func _input(event):
	if event.is_action_pressed("console"):
		var panel = $Panel
		
		if panel.visible:
			panel.hide()
		else:
			panel.show()
	
	if event.is_action_pressed("throw_grenade_right"):
		Events.emit_signal("throw_grenade", "right")
	
	if event.is_action_pressed("throw_grenade_left"):
		Events.emit_signal("throw_grenade", "left")
