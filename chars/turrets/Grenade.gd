extends Area2D

var origin_x;
var origin_y;
var bezier_p0: Vector2
var bezier_p1: Vector2
var temp_p0: Vector2
var temp_p1: Vector2
var duration = 0.01
var duration_increase = 0.001
var explodes: bool = false
var is_throwing: bool = false
var target
var init_pos
var margin_proximity = 5

# Called when the node enters the scene tree for the first duration.
func _ready():
	target.x = target.x + init_pos.x
	target.y = target.y + init_pos.y
	position = Vector2(target.x + init_pos.x, target.y + init_pos.y)
	origin_x = init_pos.x
	origin_y = init_pos.y
	bezier_p0 = Vector2(origin_x, origin_y)
	position = bezier_p0
	bezier_p1 = get_bezier_p1()

func _physics_process(delta):
	$Sprite.rotate(PI/8)
	bezier()
	
func get_bezier_p1():
	var distance = abs(origin_x - target.x)

	# target on left or right side of screen
	var posx = distance / 2
	if target.x < origin_x: # if x is on the left side
		posx = -1 * posx

	# most upper y plus 100 additional pixels to draw parabola
	var posy = min(target.y, origin_y) - 100

	return Vector2(origin_x + posx, posy)

func bezier():
	duration = duration + duration_increase
	temp_p0 = bezier_p0.linear_interpolate(bezier_p1, duration)
	temp_p1 = bezier_p1.linear_interpolate(target, duration)
	position = temp_p0.linear_interpolate(temp_p1, duration)
	bezier_p0 = temp_p0
	bezier_p1 = temp_p1

	if !explodes:
		# if it is close enough to target, stop
		if abs(bezier_p0.x - target.x) < margin_proximity && abs(bezier_p0.y - target.y) < margin_proximity:
			$Sprite.hide()
			$Explosion.emitting = true;
			explodes = true
			check_damage()
			$Timer.start()

func _on_Timer_timeout():
	$Explosion.emitting = false;
	explodes = false
	
	queue_free()
	
func check_damage():
	var areas = get_overlapping_areas()
			
	for area in areas:
		if area.is_in_group("enemy"):
			area.get_parent().receive_shot(50)
