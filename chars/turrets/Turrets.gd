extends Node2D

onready var grenade = load("res://chars/turrets/Grenade.tscn")

var deadZone: float = 0.3
var speed:int = 300
var device_0:int = 0
var warning_time = 1;

func _ready():
	Events.connect("throw_grenade", self, "_on_throw_grenade")
	$gunner_left/heat_bar.value = 0
	
func _process(delta):
	var no_enemies = true
	
	for area in $base.get_overlapping_areas():
		if area.is_in_group("enemy"):
			no_enemies = false
			$Life_gauge.value += 0.05
	
	if no_enemies:
		$Label.visible = false
		$Label/warning.stop()

func _physics_process(delta):
	if Input.get_connected_joypads().size() > 0:
		read_stick("left", delta)
		read_stick("right", delta)
		read_trigger("left", delta)
		read_trigger("right", delta)
	
	get_node("/root/Level/Control_keys/Panel/HBoxContainer/axis_values/axis_0").text = str(get_node("/root/Level/YSort/Turrets/gunner_left/gun_sight").position.x)
	get_node("/root/Level/Control_keys/Panel/HBoxContainer/axis_values/axis_1").text = str(get_node("/root/Level/YSort/Turrets/gunner_left/gun_sight").position.y)
	get_node("/root/Level/Control_keys/Panel/HBoxContainer/axis_values/axis_2").text = str(get_node("/root/Level/YSort/Turrets/gunner_right/gun_sight").position.x)
	get_node("/root/Level/Control_keys/Panel/HBoxContainer/axis_values/axis_3").text = str(get_node("/root/Level/YSort/Turrets/gunner_right/gun_sight").position.y)
	
func read_stick(side, delta):
	var gunner
	var axis_1
	var axis_2
	
	match side:
		"left":
			gunner = $gunner_left
			axis_1 = JOY_AXIS_0
			axis_2 = JOY_AXIS_1
		"right":
			gunner = $gunner_right
			axis_1 = JOY_AXIS_2
			axis_2 = JOY_AXIS_3
	
	var axis_x = Input.get_joy_axis(device_0, axis_1)
	var axis_y = Input.get_joy_axis(device_0, axis_2)
	
	move_sight(delta * speed, axis_x, axis_y, gunner)
	
func move_sight(delta:float, axis_x:float, axis_y:float, gunner):
	if abs(axis_x) < deadZone:
		axis_x = 0
		
	if abs(axis_y) < deadZone:
		axis_y = 0

	var sight = gunner.find_node("gun_sight")
	
	if sight.position.x > 527:
		sight.position.x = 527
	elif sight.position.x < -465:
		sight.position.x = -465
	else:
		sight.position.x += delta * axis_x
		
	if sight.position.y > 316:
		sight.position.y = 316
	elif sight.position.y < -252:
		sight.position.y = -252
	else:
		sight.position.y += delta * axis_y
	
	var pos_normalized = Vector2(sight.position.x, sight.position.y).normalized()
	
	set_gunner_pos(gunner, rad2deg(pos_normalized.angle()))
	
func read_trigger(side, delta):
	var gunner
	
	match side:
		"left":
			gunner = $gunner_left
		"right":
			gunner = $gunner_right
	
	if Input.is_action_pressed("shoot_" + side):
		Events.set_shooting(side, true)
	
		if gunner.find_node("heat_bar").value >= 100:
#			# SIGNAL WAIT X SECONDS
			gunner.find_node("ammo").visible = false
		else:
			gunner.find_node("ammo").visible = true
			gunner.find_node("heat_bar").value += 10 * delta
		
#			if !gunner.find_node("gun_sight/shot").is_playing():
#				gunner.find_node("gun_sight/shot").play()
			
			var areas = gunner.find_node("gun_sight").get_overlapping_areas()
			
			for area in areas:
				if area.is_in_group("enemy"):
					shoot_enemy(area.get_parent(), delta)
	else:
		# recover heat bar
		gunner.find_node("heat_bar").value -= 100 * delta
			
	if Input.is_action_just_released("shoot_left"):
		Events.set_shooting(side, false)
		gunner.find_node("ammo").visible = false

func shoot_enemy(node, delta):
	node.receive_shot(25 * delta)

func set_gunner_pos(gunner, grades):
	if grades < 22.5 && grades >= -22.5:
		gunner.find_node("sprite").frame = 0
		gunner.find_node("ammo").position.x = 3
		return
	elif grades < -22.5 && grades > -77.5:
		gunner.find_node("sprite").frame = 1
		gunner.find_node("ammo").position.x = 3
		return
	elif grades < -77.5 && grades > -102.5:
		gunner.find_node("sprite").frame = 2
		gunner.find_node("ammo").position.x = 0
		return
	elif grades < -102.5 && grades > -142.5:
		gunner.find_node("sprite").frame = 3
		gunner.find_node("ammo").position.x = -2
		return
	elif grades < -142.5 || grades > 142.5:
		gunner.find_node("sprite").frame = 4
		gunner.find_node("ammo").position.x = -3
		return
	elif grades < 142.5 && grades > 102.5:
		gunner.find_node("sprite").frame = 5
		gunner.find_node("ammo").position.x = -2
		return
	elif grades < 102.5 && grades > 77.5:
		gunner.find_node("sprite").frame = 6
		gunner.find_node("ammo").position.x = -2
		return
	elif grades < 77.5 && grades > 22.5:
		gunner.find_node("sprite").frame = 7
		gunner.find_node("ammo").position.x = 2
		return


func _on_warning_timeout():
	match warning_time:
		1:
			$Label.set("custom_colors/font_color", Color(1, 0, 1, 1))
			warning_time = 0
		0:
			$Label.set("custom_colors/font_color", Color(0, 0, 0, 1))
			warning_time = 1

func _on_base_area_entered(area):
	#set audio alarm
	
	#set visual alarm is not visible
	if $Label/warning.is_stopped():
		$Label.visible = true
		$Label/warning.start()

func _on_throw_grenade(side):
	if side == "left" && !Events.grenade_left:
		Events.grenade_left = true
		var grenade_obj = grenade.instance()
		grenade_obj.init_pos = get_node("/root/Level/YSort/Turrets/gunner_left").position
		grenade_obj.target = get_node("/root/Level/YSort/Turrets/gunner_left/gun_sight").position
		add_child(grenade_obj)
		$gunner_left/can_throw_grenade_left.start()
	
	if side == "right" && !Events.grenade_right:
		Events.grenade_right = true
		var grenade_obj = grenade.instance()
		grenade_obj.init_pos = get_node("/root/Level/YSort/Turrets/gunner_right").position
		grenade_obj.target = get_node("/root/Level/YSort/Turrets/gunner_right/gun_sight").position
		add_child(grenade_obj)
		$gunner_right/can_throw_grenade_right.start()

func _on_can_throw_grenade_left_timeout():
	Events.grenade_left = false

func _on_can_throw_grenade_right_timeout():
	Events.grenade_right = false
