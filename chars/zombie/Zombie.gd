extends KinematicBody2D

enum { INIT, APPROACH, ATTACK, DEAD }

var direction
var normalized
var speed:int
var shot_speed:int
var rand = RandomNumberGenerator.new()
var state = INIT
var time_passed = 0
var time_for_one_call = 1 / 10
var life = 8
var points = 10

func _ready():
	state = APPROACH
	
	set_initial_speed()
	set_initial_position()

func _physics_process(delta):
	if state == APPROACH:
		time_passed += delta
		
		if time_passed >= time_for_one_call:
			move_and_slide(get_direction_normalized())

#func _process(delta):
#	if state == APPROACH && get_slide_count() > 0 && get_slide_collision(0).collider.is_in_group("player"):
#		state = ATTACK

func set_initial_speed() -> void:
	rand.randomize()
	speed = rand.randi_range(25, 100)
	shot_speed = speed
	$animation.set_speed_scale(speed / 20)

func set_initial_position() -> void:
	var area = rand.randi_range(0, 3)
	
	match area:
		0:  # right
			position.x = 520
			position.y = rand.randi_range(-300, 300)
		1:  # down
			position.x = rand.randi_range(-512, 512)
			position.y = 320
		2:  # left
			position.x = -520
			position.y = rand.randi_range(-300, 300)
		3:  # top
			position.x = rand.randi_range(-512, 512)
			position.y = -320
	
	if position.x > 0:
		$animation.flip_h = true

func get_direction_normalized() -> Vector2:
	direction = Vector2(-position.x, -position.y)
	normalized = direction.normalized()
	normalized.x *= min(speed, shot_speed)
	normalized.y *= min(speed, shot_speed)
	
	return normalized

func _on_hurt_area_entered(area):
	# if arrives at player base
	if state == APPROACH && area.is_in_group("player"):
		state = ATTACK
		modulate = Color(1, 0, 0)

func receive_shot(damage):
	# show blood particles2D
	$blood_particles.show()
	# start timeout to hide blood
	$hide_blood.start()
	
	life -= damage
	
	# alter enemy speed
	# for big enemies, decrease it
	# for small enemies, stop it
#	if shot_speed > 50: 
	shot_speed = 0
	
	if life < 1:
		die()

func die():
	$die.start()
	state = DEAD
	$collision.hide()
	$hurt.hide()
	$blood_particles.hide()
	#$sprite.frame = 1
	$Tween.interpolate_property(
		self,
		"modulate",
		null,
		Color(1, 1, 1, 0),
		1.0,
		Tween.TRANS_LINEAR,
		Tween.EASE_IN_OUT
	)
	$Tween.start()

func _on_hide_blood_timeout():
	$blood_particles.hide()
	# restore original speed
	shot_speed = speed

func _on_die_timeout():
	queue_free()
	Events.modify_enemy_count(-1)
